package trainsystem;

import java.util.ArrayList;
import java.util.Scanner;

import THSR.AssistBook;
import THSR.Booking;
import THSR.BookingException;
import THSR.TrainTable;

/**
 * This class allows the user the change or remove the reservation he/she made.
 * 
 * @author Raymond
 *
 */
public class Change {
	public static void change(ArrayList<TrainTable> tt) {
		System.out.println("To cancel a reservation, enter 1. To reduce the number of tickets, enter 2.");
		@SuppressWarnings("resource")
		Scanner entry = new Scanner(System.in);
		int act = entry.nextInt();

		switch (act) {
		case 1:
			cancel();
			break;
		case 2:
			reduce(tt);
			break;
		}
	}

	/**
	 * Removes a reservation.
	 */
	static void cancel() {
		IOjson.remove();
	}

	/**
	 * Rebook.
	 * 
	 * @param tt
	 *            The BIG Train Table.
	 */
	static void reduce(ArrayList<TrainTable> tt) {
		IOjson.remove();
		AssistBook theBook;
		try {
			theBook = new AssistBook(tt);
			Booking.book(theBook);
		} catch (BookingException e) {
			e.printStackTrace();
		}

	}

}
