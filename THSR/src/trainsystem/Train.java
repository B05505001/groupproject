package trainsystem;
import java.util.Calendar;
import THSR.TrainType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
/**
 * <h1>Train</h1>
 * <p>The unit of the TrainTable.
 * 
 * @author b05505001
 * @version 0.1
 * @since 2018-05-31
 */
public class Train {
	String trainNo;
	TrainType type;
	int direction;
	Station startEnd;
	Stop[] stop;
	String dateDepart;
	Calendar dateDepartC = Calendar.getInstance();
	
	/**
	 * Set the train number and construct the array of Stops.
	 * @param stn The train number to set to.
	 */
	public void setTrainNo(String stn) {
		trainNo = stn;
		String temp = stn.substring(1, 2);
		if(temp.equals("6")) {
			type = TrainType.T6;
			stop = new Stop[9];
			for(int i=0;i<9;i++)stop[i] = new Stop();
		}
		else if(temp.equals("8")) {
			type = TrainType.T8;
			stop = new Stop[12];
			for(int i=0;i<12;i++)stop[i] = new Stop();
		}
		else if(temp.equals("1")) {
			type = TrainType.T1;
			stop = new Stop[5];
			for(int i=0;i<5;i++)stop[i] = new Stop();
		}
		else if(temp.equals("2")) {
			type = TrainType.T2;
			stop = new Stop[7];
			for(int i=0;i<7;i++)stop[i] = new Stop();
		}
		else if(temp.equals("5")) {
			type = TrainType.T5;
			stop = new Stop[7];
			for(int i=0;i<7;i++)stop[i] = new Stop();
		}
		else if(temp.equals("3")) {
			type = TrainType.T3;
			stop = new Stop[9];
			for(int i=0;i<9;i++)stop[i] = new Stop();
		}
		else {
			System.out.println("No such type of train!");
		}
		
	}
	
	/**
	 * Set the direction.0 means from North to South.1 means from South to North.
	 * @param d The direction to set to.
	 */
	public void setDirection(int d) {
		direction = d;
	}
	
	/**
	 * Set the starting and ending station.
	 * @param start Starting station.
	 * @param end Ending station.
	 */
	public void setStartEnd(String start,String end) {
		startEnd = new Station(start,end);
	}
	
	/**
	 * Set the start and end stations of the train.
	 * @param S The start and end stations.
	 */
	public void setStartEnd(Station S) {
		startEnd = new Station(S);
		
	}
	
	/**
	 * Set the start date of departure date.
	 * @param string Start date of effectiveDateDepart.
	 */
	public void setDateDepart(String departD) {
		SimpleDateFormat temp=new SimpleDateFormat("yyyy-MM-dd");
		dateDepart = departD;
		try {
			dateDepartC.setTime(temp.parse(departD));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Set the station to stop and the departure time from the station.
	 * @param stationS The station to stop.
	 * @param depart The departure time from the station.
	 * @param index The sequence of the station.
	 */
	public void setStop(String stationS,String departT,int index) {
		stop[index].setStation(stationS);
		stop[index].setDepartTime(dateDepart + " " + departT);
	}

	/**
	 * Set this Stop array by point to the address of the Stop array allStop. 
	 * @param allStop (Better be the new one or create by the clone method.)
	 */
	public void setAllStop(Stop[] allStop) {
		stop = allStop;
	}
	
	/**
	 * Return the trainNo.
	 * @return A String object stores the number of the train.
	 */
	public String getTrainNo() {
		return trainNo;
	}
	
	/**
	 * Return the type of the train.
	 * @return The type of the train.
	 */
	public TrainType getTrainType() {
		return type;
	}
	
	/**
	 * Return the stop of the sequence.
	 * @param index The sequence of the stop.
	 * @return Stop of the sequence.
	 * @throws CloneNotSupportedException This method will use the clone method.
	 */
	public Stop getStop(int index) throws CloneNotSupportedException {
		return stop[index].clone();
	}
	
	/**
	 * Return the direction. 
	 * @return Direction.0 means from North to South.1 means from South to North.
	 */
	public int getDirection() {
		return direction;
	}
	
	/**
	 * Return the departure date of this train.
	 * @return Calendar departure date of the train.
	 */
	public Calendar getDepartDate() {
		Calendar temp = Calendar.getInstance();
		temp.setTime(dateDepartC.getTime());
		return temp;
	}
	
	/**
	 * Return a new Stop array copy from this Stop array.
	 * @return A new Stop array copy from this Stop array.
	 */
	public Stop[] getAllStop() {
		Stop[] temp = new Stop[stop.length];
		try {
			for(int i=0;i<temp.length;i++) {
				temp[i] = stop[i].clone(); 
			}
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		} 
		return temp;
	}
	
	/**
	 * Return number of Stops.
	 * @return Number 0f Stops.
	 */
	public int getNoOfStop() {
		return stop.length;
	}
	
	/**
	 * Clone method of the Train object.
	 */
	public Train clone() {
		Train temp = new Train();
		temp.setTrainNo(this.trainNo);
		temp.setDirection(this.direction);
		temp.setStartEnd(this.startEnd);
		temp.setAllStop(this.getAllStop());
		temp.setDateDepart(dateDepart);
		return temp;
	}

	/**
	 * Return the informations(TrainNo,Direction,StartingStation,EndingStation) of the train.
	 */
	public String toString() {
		return "Train No:"+trainNo+"\nDirection:"+direction+","+startEnd;
	}
}

