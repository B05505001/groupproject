package trainsystem;

import java.util.Scanner;

/**
 * Stores all stations with emun. Has methods setOrigin, setDestination.
 * 
 * @author Raymond&Po-Sung
 *
 */
public class Station {
	public enum station {
		Nangang, Taipei, Banciao, Taoyuan, Hsinchu, Miaoli, Taichung, Changhua, Yunlin, Chiayi, Tainan, Zuoying
	}

	public station station1;
	public station station2;

	public Station() {
		station1 = station.Nangang;
		station2 = station.Zuoying;
	}

	public Station(Station S) {
		this.station1 = S.station1;
		this.station2 = S.station2;
	}

	public Station setStation(Station S) {
		this.station1 = S.station1;
		this.station2 = S.station2;
		return this;
	}

	public station getOrigin() {
		return station1;
	}

	public station getDestination() {
		return station2;
	}

	public int getOriginOrdinal() {
		return station1.ordinal();
	}

	public int getDestinationOrdinal() {
		return station2.ordinal();
	}

	public Station(String originS, String destinationS) {
		station1 = station.valueOf(originS);
		station2 = station.valueOf(destinationS);
	}

	public Station setStation(String originS, String destinationS) {
		station1 = station.valueOf(originS);
		station2 = station.valueOf(destinationS);
		return this;
	}

	public String toString() {
		return "Destination: " + station2.toString() + ", Origin: " + station1.toString();
	}

	public static Station IOset() {
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		System.out.print(
				"Select Origin:\n1.Nangang,2.Taipei,3.Banqiao,4.Taoyuan,5.Hsinchu,6.Miaoli,\n7.Taichung,8.Changhua,"
						+ "9.Yunlin,10.Chiayi,11.Tainan,12.Zuoying:");
		int originI;
		do {
			originI = input.nextInt();
			if (originI > 12 || originI < 0)
				System.out.println("Your request is illegal!");
		} while (originI > 12 || originI < 0);
		String originS;
		switch (originI) {
		case 1:
			originS = "Nangang";
			break;
		case 2:
			originS = "Taipei";
			break;
		case 3:
			originS = "Banciao";
			break;
		case 4:
			originS = "Taoyuan";
			break;
		case 5:
			originS = "Hsinchu";
			break;
		case 6:
			originS = "Miaoli";
			break;
		case 7:
			originS = "Taichung";
			break;
		case 8:
			originS = "Changhua";
			break;
		case 9:
			originS = "Yunlin";
			break;
		case 10:
			originS = "Chiayi";
			break;
		case 11:
			originS = "Tainan";
			break;
		case 12:
			originS = "Zuoying";
			break;
		default:
			System.out.println("Error!");
			throw new RuntimeException("unreachable");
		}
		System.out.print(
				"Select Destination:\n1.Nangang,2.Taipei,3.Banqiao,4.Taoyuan,5.Hsinchu,6.Miaoli,\n7.Taichung,8.Changhua,"
						+ "9.Yunlin,10.Chiayi,11.Tainan,12.Zuoying:");
		int destinationI;
		do {
			destinationI = input.nextInt();
			if (destinationI > 12 || destinationI < 0)
				System.out.println("Your request is illegal!");
		} while (destinationI > 12 || destinationI < 0);
		String destinationS;
		switch (destinationI) {
		case 1:
			destinationS = "Nangang";
			break;
		case 2:
			destinationS = "Taipei";
			break;
		case 3:
			destinationS = "Banciao";
			break;
		case 4:
			destinationS = "Taoyuan";
			break;
		case 5:
			destinationS = "Hsinchu";
			break;
		case 6:
			destinationS = "Miaoli";
			break;
		case 7:
			destinationS = "Taichung";
			break;
		case 8:
			destinationS = "Changhua";
			break;
		case 9:
			destinationS = "Yunlin";
			break;
		case 10:
			destinationS = "Chiayi";
			break;
		case 11:
			destinationS = "Tainan";
			break;
		case 12:
			destinationS = "Zuoying";
			break;
		default:
			System.out.println("Error!");
			throw new RuntimeException("unreachable");
		}
		return new Station(originS, destinationS);
	}
}
