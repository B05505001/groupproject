/**
 * 
 */
/**
 * Consists of many static methods to setup a train, essential for reservation.
 * 
 * @author Raymond
 *
 */
package trainsystem;