package trainsystem;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import trainsystem.Station.station;
/**
 * 
 * <h1>Stop</h1>
 * <p>This class has two member of station type and Date type.It is used to 
 * store the station to stop and the departure time from the station.
 * @author b05505001
 * @version 1.0
 * @since 2018-06-05 
 */
public class Stop {
	station S;
	Calendar departureTime = Calendar.getInstance(); 
	
	/**
	 * Set the station .
	 * @param S1 The station to set to.
	 */
	public void setStation(String S1) {
		S = station.valueOf(S1);
	}
	
	/**
	 * Set the departure time from the station.
	 * @param depart The departure time.
	 */
	public void setDepartTime(String depart) {
		SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date timeD;
		try {
			timeD = time.parse(depart);
			departureTime.setTime(timeD);
		} catch (ParseException e) {
			e.printStackTrace();
		} 
	}
	
	/**
	 * Return this station type member.
	 * @return The station object copy from this station member.
	 */
	public station getStation() {
		return S;
	}
	
	/**
	 * Return the Calendar with information of departureTime.
	 * @return The Calendar with information of departureTime.
	 */
	public Calendar getDepartureTime() {
		Calendar tempC = Calendar.getInstance();
		Date tempD = departureTime.getTime();
		tempC.setTime(tempD);
		return tempC; 
	}
	
	/**
	 * Return the departure time in the form of "yyyy-MM-dd HH:mm".
	 * @return The departure time in the form of "yyyy-MM-dd HH:mm".
	 */
	public String departToString() {
		SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date temp = departureTime.getTime();
		return time.format(temp);
	}
	
	/**
	 * Clone the Stop object and return it.
	 */
	public Stop clone()throws CloneNotSupportedException {
		Stop temp = new Stop();
		temp.setDepartTime(this.departToString());
		if(S!=null)temp.setStation(this.S.toString());
		return temp;
		
	}
	
	/**
	 * Return the Stop information in the form of "Station:S\nDeparture Time:yyyy-MM-dd HH:mm\n".
	 */
	public String toString() {
		if(S==null)return "Station:\nDeparture Time:\n";
		return "Station:"+this.getStation()+"\nDeparture Time:"+this.departToString()+"\n";
	}
}
