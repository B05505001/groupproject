package trainsystem;

/**
 * @author B05505033 Benson Weng
 * @since 2018/05/10
 * @version 1.1
 <h1>This class represents a situation</h1>
 <p>Change Log<br>
 1.0: Generated this class<br>
1.1: Changed all Chinese characters into English
 *
 */
public enum Situation {
window,
aisle,
none
}
