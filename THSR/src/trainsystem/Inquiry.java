package trainsystem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class is the inquiry system, which provides the user of their booking
 * history or Reservation number in case they forget.
 * 
 * @author B05505024 Raymond
 *
 */
public class Inquiry {
	/**
	 * Starts the enquiry process, choosing between two other methods. Find Booking
	 * History or Find Reservation Number.
	 */
	public static void ask() {
		System.out.println("For Booking History, enter 1. To Enquire Reservation number, enter 2.");
		@SuppressWarnings("resource")
		Scanner entry = new Scanner(System.in);
		int act = entry.nextInt();

		switch (act) {
		case 1:
			findHistory();
			break;
		case 2:
			findReservationNo();
			break;
		}
	}

	/**
	 * Find the user's booking history. Prints his/her reservation details on
	 * screen.
	 */
	private static void findHistory() {
		boolean found = false;
		System.out.println("Please enter your UID, and Reservation number");
		System.out.println("UID:");
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		String UID = input.next();
		System.out.println("Reservation number:");
		int RN = input.nextInt();
		JSONObject tempJO = new JSONObject();
		JSONObject Jfile = IOjson.read();
		JSONArray RJA = Jfile.getJSONArray("Reservations");
		for (int i = 0; i < RJA.length(); i++) {
			tempJO = RJA.getJSONObject(i);
			if (tempJO.getString("ID").equals(UID) && tempJO.getInt("OrderNo") == RN) {
				System.out.println(tempJO);
				found = true;
				break;
			}
		}
		if (!found)
			System.out.println("Could not find corresponding book history.");
	}

	/**
	 * Finds the user's Reservation number with entered Reservation details. Prints
	 * the Reservation number on screen.
	 */
	private static void findReservationNo() {
		boolean found = false;
		Station S;
		Calendar C = new GregorianCalendar();
		String Train;

		System.out.println("Please enter your UID, Itinerary, Outbound Date, and Train ID No.");
		System.out.println("UID:");
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		String ID = input.next();
		S = Station.IOset();
		System.out.println("Departure Date and Time:");
		System.out.print("Month:");
		int month = input.nextInt() - 1;
		System.out.print("Day:");
		int day = input.nextInt();
		System.out.print("Year:");
		int year = input.nextInt();
		C.clear();
		C.set(year, month, day);
		System.out.println("Train Number (include zeros):");
		Train = input.next();
		JSONObject temp = new JSONObject();
		JSONObject reservationN = IOjson.read();
		JSONArray ticketList = reservationN.getJSONArray("Reservations");
		SimpleDateFormat d = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
		for (int i = 0; i < ticketList.length(); i++) {
			temp = ticketList.getJSONObject(i);
			Date dt;
			try {
				dt = (Date) d.parse(temp.getString("DepartureTime"));
				System.out.println(dt);
				Calendar Ct = Calendar.getInstance();
				Calendar CR = Calendar.getInstance();
				Ct.setTime(dt);
				CR.clear();
				CR.set(Ct.get(Calendar.YEAR), Ct.get(Calendar.MONTH), Ct.get(Calendar.DATE));
				if (temp.getString("ID").equals(ID) && temp.getString("TrainNo").equals(Train)
						&& CR.toString().equals(C.toString()) && temp.get("Station").toString().equals(S.toString())) {
					found = true;
					System.out.println(temp.getInt("OrderNo"));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		if (!found)
			System.out.println("Could not find the Reservation.");

	}
}
