package THSR;

import java.util.Calendar;
import trainsystem.Station;
import trainsystem.Train;
/**
 * <h1>TrainMatch</h1>
 * <p>This class provide some methods to match the info of the trains. 
 * @author B05505001
 * @version 1.0
 * @since 2018-6-17
 *
 */
public class TrainMatch {
	
	/**
	 * Match the train informations of time,direction and Station.
	 * @param t The train to match.
	 * @param departTime The time to match.
	 * @param direc The direction to match.
	 * @param DAndO The destination and departure.
	 * @return true(match);false(Not match) 
	 */
	public static boolean trainMatch(Train t,Calendar departTime,int direc,Station DAndO) {
		boolean destFon = false,orgFon = false;
		Calendar before2Hours = Calendar.getInstance(),after2Hours = Calendar.getInstance();
		before2Hours.setTime(departTime.getTime());
		after2Hours.setTime(departTime.getTime());
		before2Hours.add(Calendar.HOUR, -2);
		after2Hours.add(Calendar.HOUR, +2);
		if(direc==t.getDirection()) {
			for(int i=0;i<t.getNoOfStop();i++) {
				try {
					if(DAndO.getOrigin().equals(t.getStop(i).getStation())&&t.getStop(i).getDepartureTime().after(before2Hours)&&t.getStop(i).getDepartureTime().before(after2Hours))orgFon = true;
					if(DAndO.getDestination().equals(t.getStop(i).getStation()))destFon = true;
				} 
				catch (CloneNotSupportedException e1) {
					e1.printStackTrace();
				}
				
			}
		}
		return destFon&&orgFon; 
	}
	
	/**
	 * Testing.
	 * @param t null.
	 * @param DAndO null.
	 * @return
	 */
	public static Calendar getMatchDepartTime(Train t,Station DAndO) {
		Calendar temp = Calendar.getInstance();
		for(int i=0;i<t.getNoOfStop();i++) {
			try {
				if(DAndO.getOrigin().equals(t.getStop(i).getStation())) {
					temp = t.getStop(i).getDepartureTime();
				}
			} 
			catch (CloneNotSupportedException e1) {
				e1.printStackTrace();
			}	
		}
		return temp;
	}
	
	/**
	 * Testing.
	 * @param t null.
	 * @param DAndO null.
	 * @return null.
	 */
	public static Calendar getMatchArriveTime(Train t,Station DAndO) {
		Calendar temp = Calendar.getInstance();
		for(int i=0;i<t.getNoOfStop();i++) {
			try {
				if(DAndO.getDestination().equals(t.getStop(i).getStation())) {
					temp = t.getStop(i).getDepartureTime();
				}
			} 
			catch (CloneNotSupportedException e1) {
				e1.printStackTrace();
			}	
		}
		return temp;
	}
}
