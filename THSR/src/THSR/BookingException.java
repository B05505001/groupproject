package THSR;

/**
 * <h1>BookingException</h1>
 * <p>This class is used to handle the exception occur from the process 
 * of booking.
 * 
 * @author b05505001
 * @version 1.0
 * @since 2018-06-28
 */
@SuppressWarnings("serial")
public class BookingException extends Exception {
	/**
	 * Default constructor of the BookingException.
	 */
	public BookingException() {
		super("Return fallen wings to the sky.");
	}
	/**
	 * Constructor of the BookingException with an error message sent in.
	 * @param errMessage The error message.
	 */
	public BookingException(String errMessage) {
		super(errMessage);
	}
	/**
	 * Return the error message to let the user know why the system would fail.
	 */
	public String getMessage() {
		return super.getMessage();
	}
}
