package THSR;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONObject;

import trainsystem.Change;
import trainsystem.IOjson;
import trainsystem.Inquiry;

/**
 * <h1>Booking</h1>
 * <p>
 * The main system of this THSR booking program.
 * 
 * @author b05505001&B05505024
 * @version 1.0
 * @since 2018-06-24
 */
public class Booking {

	/**
	 * Let the user choose the function he/she want to use.
	 */
	public void getAccess(ArrayList<TrainTable> tt) {
		Scanner keyboard = new Scanner(System.in);
		boolean access = true;
		System.out.println("Welcome to the THSR Booking System!What can I help you?");
		while (access) {
			System.out.println("=============================================");
			System.out.println("Please choose the service you want.");
			System.out.print(
					"1.Make a ticket. | 2.Booking History. | 3.Change the ticket. | 4.Check the Train table. | 5.Leave.\nAction:");
			int act = keyboard.nextInt();
			switch (act) {
			case 1:
				try {
					AssistBook theBook = new AssistBook(tt);
					book(theBook);
					System.out.println("Booking successful!");
				} catch (BookingException e) {
					System.out.println("Booking failed!");
					System.out.println("Reason:" + e.getMessage());
				}
				break;
			case 2:
				Inquiry.ask();
				break;
			case 3:
				Change.change(tt);
				break;
			case 4:
				AssistBook.CheckTrainTable(keyboard, tt);
				break;
			case 5:
				System.out.println("May the wind carry your wings!");
				access = false;
				break;
			default:
				System.out.println("May the wind carry wings that are lost!");
				break;
			}
		}
	}

	/**
	 * This method will help the booking system create a json object with the data
	 * of a reservation and store/write it to the test.json file.
	 * @param assist
	 *            The assist book object.
	 */
	public static void book(AssistBook assist) {
		JSONObject obj = new JSONObject();
		obj.put("ID", assist.getUID());
		obj.put("Name", assist.getName());
		obj.put("Phone", assist.getPhone());
		obj.put("Station", assist.getSAD());
		obj.put("Class", assist.getTicketType());
		obj.put("Seat", assist.getSeats());
		obj.put("TrainNo", assist.getTrainNo());
		obj.put("DepartureTime", assist.getDepartTime());
		JSONArray JA = (JSONArray) IOjson.read().get("Reservations");
		obj.put("OrderNo", JA.length());
		System.out.println("Your Order Number is: " + String.format("%05d", JA.length()));
		IOjson.add(obj);
	}

	/**
	 * This method will produce all the traintables within 28days from "now".
	 * 
	 * @return The produce TrainTables stores by an ArrayList<TrainTable>.
	 */
	public static ArrayList<TrainTable> produceAllTrain() {
		ArrayList<TrainTable> allTable = new ArrayList<TrainTable>();
		Calendar date = Calendar.getInstance();
		TrainTable oneDayTable;
		for (int i = 0; i < 28; i++) {
			oneDayTable = new TrainTable(ProduceOneDay.produce("json/timeTable.json", date), date);
			allTable.add(oneDayTable);
			date.add(Calendar.DATE, 1);
		}
		return allTable;
	}

}
