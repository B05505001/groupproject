/**
 * 
 */
/**
 * This package includes classes for initialization of the json file which
 * stores reservations and the primary user interface. For some unknown reason
 * this package also includes methods for the entire booking process.
 * 
 * @author Raymond
 *
 */
package THSR;