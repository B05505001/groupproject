package THSR;

import java.io.*;
import java.text.*;
import java.util.*;
import org.json.*;
import trainsystem.*;

/**
 * <h1>AssistBook</h1>
 * <p>This class is used to assist the Booking System to generate an object
 * that receive the messages from the user and give the respond.  
 * 
 * @author b05505001
 * @version 1.0
 * @since 2018-06-28
 */
public class AssistBook {
	public enum Type{
		standard,
		business,
		student,
	}
	JSONArray books = new JSONArray();
	String uid;
	String name;
	long phone;
	int numOfSeats;
	Type typeOfTicket;
	Station startAndDest;
	Train yourTrain = new Train();
	int dir;
	Calendar departTime = Calendar.getInstance();
	Situation s;
	ArrayList<String> seats;
	
	/**
	 * Constructor of the assistBook.
	 * @param tt The TrainTable produced from the using date.
	 * @throws BookingException To make the user knows why the booking failed.
	 */
	public AssistBook(ArrayList<TrainTable> tt) throws BookingException {
		Scanner keyboard = new Scanner(System.in);
		this.chooseTicketType(keyboard);
		this.chooseStation();
		this.inputDepartTime(keyboard);
		ArrayList<Train> tl = this.findMatchTrain(tt);
		this.chooseTrain(keyboard, tl);
		this.chooseSeatPrefer(keyboard);
		this.inputNumofSeats(keyboard);
		seats = assignSeat(yourTrain.getTrainNo(), departTime, s, typeOfTicket, numOfSeats);
		System.out.println("Your seat:" + seats);
		this.inputProfile(keyboard);
	}
	
	/**
	 * Copy constructor of the AssistBook.
	 * @param temp The other AssistBook object to copy.
	 */
	public AssistBook(AssistBook temp) {
		this.books = temp.books;
		this.name = temp.name;
		this.uid = temp.uid;
		this.phone = temp.phone;
		this.numOfSeats = temp.numOfSeats;
		this.typeOfTicket = temp.typeOfTicket;
		this.startAndDest = temp.startAndDest;
		this.yourTrain = temp.yourTrain;
		this.dir = temp.dir;
		this.departTime = temp.departTime;
		this.s = temp.s;
		this.seats = temp.seats;
	}
	/**
	 * Return the User Identifier.
	 * @return The String stores the User Identifier.
	 */
	public String getUID() {
		return uid;
	}
	
	/**
	 * Return the User's Name.
	 * @return The String stores the User's Name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Return the user's phone number.
	 * @return The long variable stores the user's phone number.
	 */
	public long getPhone() {
		return phone;
	}
	
	/**
	 * Return the user's origin and destination.
	 * @return The Station object that stores the user's origin and destination.
	 */
	public Station getSAD() {
		return new Station(startAndDest);
	}
	
	/**
	 * Return the user's Ticket type.
	 * @return The String object that stores stores the user's t.
	 */
	public String getTicketType() {
		return typeOfTicket.toString();
	}
	
	/**
	 * Return the seats that user been assigned.
	 * @return An ArrayList with String as base type that stores the seats info that 
	 * user been assigned.
	 */
	public ArrayList<String> getSeats(){
		return new ArrayList<String>(seats);
	}
	
	/**
	 * Return the Train number of the train that the user choose.
	 * @return The String object that stores the Train number.
	 */
	public String getTrainNo() {
		return yourTrain.getTrainNo();
	}
	
	/**
	 * Return the departure time that the user choose.
	 * @return A date object that stores the departure time.
	 */
	public Date getDepartTime() {
		return departTime.getTime();
	}
	
	/**
	 * Let the user input the personal profile.
	 * @param keyboard Input.
	 */
	private void inputProfile(Scanner keyboard) {
		System.out.println("Please key in your profile.");
		System.out.print("R.O.C ID:");
		uid = keyboard.next();
		System.out.println("Your R.O.C ID:" + uid);
		System.out.print("Name:");
		name = keyboard.next();
		System.out.println("Your name:" + name);
		System.out.print("Phone number:");
		phone = keyboard.nextLong();
		System.out.println("Your phone number:" + phone);
	}

	/**
	 * Let the user input the number of seats he/she wants.
	 * @param keyboard Input.
	 */
	private void inputNumofSeats(Scanner keyboard) {
		System.out.println("Please key in the number of seats you want(<=10 or >=1).");
		do {
			numOfSeats = keyboard.nextInt();
			if (numOfSeats > 10 || numOfSeats < 1)
				System.out.println("Your request is illegal.\nPlease key in again.");
		} while (numOfSeats > 10 || numOfSeats < 1);
	}

	/**
	 * Let the user choose the type of ticket.
	 * @param keyboard Input.
	 */
	private void chooseTicketType(Scanner keyboard) {
		int type;
		System.out.print("Select Type of Ticket:\n1.Standard, 2.Business, 3.Student:");
		do {
			type = keyboard.nextInt();
			if (type > 3 || type < 0)
				System.out.println("Your request is illegal!");
		} while (type > 3 || type < 0);
		switch (type) {
		case 1:
			typeOfTicket = Type.standard;
			break;
		case 2:
			typeOfTicket = Type.business;
			break;
		case 3:
			typeOfTicket = Type.student;
			break;
		default:
			typeOfTicket = Type.standard;
			break;
		}
		System.out.println("Your ticket type:" + typeOfTicket.toString());
	}

	/**
	 * Let the user choose the origin and destination.
	 */
	private void chooseStation() {
		startAndDest = Station.IOset();
		;
		if ((startAndDest.getDestinationOrdinal() - startAndDest.getOriginOrdinal()) < 0)
			dir = 1;
		else {
			dir = 0;
		}
		System.out.println(startAndDest);
	}

	/**
	 * Let the user input the departure date and time.
	 * @param keyboard Input.
	 */
	private void inputDepartTime(Scanner keyboard) {
		boolean illegalDate;
		Calendar now = Calendar.getInstance();
		Calendar nowAdd28 = Calendar.getInstance();
		nowAdd28.add(Calendar.DATE, 27);
		System.out.println("Departure Date and Time:(From now on 28 days are availible.)");
		do {
			System.out.print("Month:");
			int month = keyboard.nextInt();
			System.out.print("Day:");
			int day = keyboard.nextInt();
			System.out.print("Year:");
			int year = keyboard.nextInt();
			System.out.print("Departure Time(24hr System):\nHour:");
			int hour = keyboard.nextInt();
			System.out.print("Minute:");
			int minute = keyboard.nextInt();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			try {
				departTime.setTime(sdf.parse(year + "-" + month + "-" + day + " " + hour + ":" + minute));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			illegalDate = departTime.after(nowAdd28) || departTime.before(now);
			if (illegalDate)
				System.out.println("Your request is illegal!");
		} while (illegalDate);
		System.out.println("Your departure date and time:" + departTime.getTime().toString());
	}

	/**
	 * Let the user choose the seat preference.
	 * @param keyboard Input.
	 */
	private void chooseSeatPrefer(Scanner keyboard) {
		System.out.print("Seat prefer:\n1.Window;2.Aisle;3.None:");
		int prefer = keyboard.nextInt();
		switch (prefer) {
		case 1:
			s = Situation.window;
			break;
		case 2:
			s = Situation.aisle;
			break;
		default:
			s = Situation.none;
			break;
		}
	}

	/**
	 * List the trains that are fit the require let the user choose.
	 * @param keyboard Input.
	 * @param tl The train list of the date the user want to departure.
	 * @throws BookingException If there isn't any fit train,throw this exception carry on
	 * an info "Sorry!There isn't any fit train for you".
	 */
	private void chooseTrain(Scanner keyboard,ArrayList<Train> tl) throws BookingException {
		ArrayList<Train> tQ = new ArrayList<Train>();
		for (Train e:tl) {
			if (TrainMatch.trainMatch(e, departTime, dir, startAndDest))tQ.add(e);
		}
		if(tQ.isEmpty())throw new BookingException("Sorry!There isn't any fit train for you.");
		SimpleDateFormat d = new SimpleDateFormat("HH:mm");
		System.out.println(" index | Train No | DepartureTime |  ArrivalTime");
		for (int i = 0; i < tQ.size(); i++) {
			if ((i+1) > 9) {
				System.out.println("   " + (i+1) + ". |   " + tQ.get(i).getTrainNo() + "   |     "
						+ d.format(TrainMatch.getMatchDepartTime(tQ.get(i), startAndDest).getTime()) + "     |     "
						+ d.format(TrainMatch.getMatchArriveTime(tQ.get(i), startAndDest).getTime()));
			} else {
				System.out.println("   " + (i+1) + ".  |   " + tQ.get(i).getTrainNo() + "   |     "
						+ d.format(TrainMatch.getMatchDepartTime(tQ.get(i), startAndDest).getTime()) + "     |     "
						+ d.format(TrainMatch.getMatchArriveTime(tQ.get(i), startAndDest).getTime()));
			}
		}
		int choose;
		System.out.println("Choose the train:");
		do {
			choose = keyboard.nextInt();
			if(choose>tQ.size()||choose<1)System.out.println("The train you choose does not exist!");
		}
		while(choose>tQ.size()||choose<1);
		for (int i = 0; i < tQ.size(); i++) {
			if (choose == (i + 1)) {
				System.out.println("Your choose:" + tQ.get(i).getTrainNo());
				yourTrain = tQ.get(i);
				break;
			}
		}
		departTime = TrainMatch.getMatchDepartTime(yourTrain, startAndDest);
	}

	/**
	 * Find the Train list of the date user want to search.
	 * @param t The TrainTable stores Train lists of 28 days from "now" on.
	 * @return An array list stores Trains of the date.
	 */
	private ArrayList<Train> findMatchTrain(ArrayList<TrainTable> t) {
		for (int i = 0; i < t.size(); i++) {
			if (t.get(i).trainTableMatch(departTime)) {
				try {
					return t.get(i).getTrainTable();
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
				}
			}
		}
		return new ArrayList<Train>();
	}
	
	/**
	 * This method will assign the seat to the passenger.
	 * @param trainNo The train number.
	 * @param departTime The departure time.
	 * @param s The seat preferance.
	 * @param typeOfTicket The type of ticket.
	 * @param numOfSeats The number of seats.
	 * @return The seat info in form likes "Car:1 Seat:1A";
	 * @throws BookingException If there are not enough fit seats,throw this exception carry on
	 * an info "Sorry!There isn't enough seats in the train".
	 */
	public static ArrayList<String> assignSeat(String trainNo,Calendar departTime, Situation s,Type typeOfTicket, int numOfSeats) throws BookingException {
		JSONArray cars, row;
		JSONObject seatArrangeO, instanceCar, seatsJ;
		ArrayList<String> seatList = new ArrayList<String>();
		String seat;
		try {
			String seatsArrange;
			BufferedReader bufferedReader = new BufferedReader(new FileReader("json/seat.json"));
			StringBuffer stringBuffer = new StringBuffer();
			while (bufferedReader.ready()) {
				stringBuffer.append(bufferedReader.readLine());
			}
			bufferedReader.close();
			seatsArrange = stringBuffer.toString();
			seatArrangeO = new JSONObject(seatsArrange);
			cars = seatArrangeO.getJSONArray("cars");
			boolean seatfound = false;
			for (int i = 0; i < cars.length(); i++) {
				instanceCar = cars.getJSONObject(i);
				if (!instanceCar.getString("type").equals(typeOfTicket.toString())) {
					if (!typeOfTicket.equals(Type.student))
						continue;
				}
				seatsJ = instanceCar.getJSONObject("seats");
				for (int j = 1; j <= seatsJ.length(); j++) {
					row = seatsJ.getJSONArray(String.valueOf(j));
					for (int k = 0; k < row.length(); k++) {
						seat = "";
						if (s.equals(Situation.window)) {
							if (row.getString(k).equals("A")) {
								seat = "Car:" + (i + 1) + " Seat:" + j + row.getString(k);
							} else if (row.getString(k).equals("E")) {
								seat = "Car:" + (i + 1) + " Seat:" + j + row.getString(k);
							} else {
								continue;
							}
						} else if (s.equals(Situation.aisle)) {
							if (row.getString(k).equals("C")) {
								seat = "Car:" + (i + 1) + " Seat:" + j + row.getString(k);
							} else if (row.getString(k).equals("D")) {
								seat = "Car:" + (i + 1) + " Seat:" + j + row.getString(k);
							} else {
								continue;
							}
						} else {
							seat = "Car:" + (i + 1) + " Seat:" + j + row.getString(k);
						}
						if (unSeated(trainNo, departTime, seat) && !seat.equals("")) {
							seatList.add(seat);
							if (seatList.size() >= numOfSeats) {
								seatfound = true;
								break;
							}
						}
					}
					if (seatfound)
						break;
				}
				if (seatfound)
					break;
			}
			if(!seatfound)throw new BookingException("Sorry!There isn't enough seats in the train."); 
		} catch (IOException e) {
			e.printStackTrace();
		}
		return seatList;
	}

	/**
	 * This method will check weather the seat is seated or not.
	 * @param seat The seat to check.
	 * @return If seated(false);not seated(true).
	 */
	private static boolean unSeated(String TrainNo,Calendar departTime, String seat) {
		boolean unSeated = true;
		SimpleDateFormat get = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
		SimpleDateFormat compare = new SimpleDateFormat("yyyy MM dd", Locale.ENGLISH);
		String temp;
		Date date;
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader("json/test.json"));
			StringBuffer stringBuffer = new StringBuffer();
			while (bufferedReader.ready()) {
				stringBuffer.append(bufferedReader.readLine());
			}
			temp = stringBuffer.toString();
			bufferedReader.close();
			JSONObject temp2 = new JSONObject(temp);
			JSONArray reservations = temp2.getJSONArray("Reservations"),temp3;
			for(int i = 0; i < reservations.length(); i++) {
				temp2 = reservations.getJSONObject(i);
				date = get.parse(temp2.getString("DepartureTime"));
				if (compare.format(departTime.getTime()).equals(compare.format(date))) {
					if (TrainNo.equals(temp2.getString("TrainNo"))) {
						temp3 = temp2.getJSONArray("Seat");
						for(int j = 0;j<temp3.length();j++) {
							if (temp3.getString(j).equals(seat)) {
								unSeated = false;
								break;
							}
						}
					}
				}
			}
		} catch (IOException | JSONException | ParseException e) {
			e.printStackTrace();
		}
		return unSeated;
	}

	
	/**
	 * Let the user check the train table of the date he/she want to check.
	 * @param keyboard Input.
	 * @param tt All the train tables produced from "now".
	 */
	public static void CheckTrainTable(Scanner keyboard,ArrayList<TrainTable> tt) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar nowAdd28 = Calendar.getInstance();
		Calendar date = Calendar.getInstance();
		Calendar now = Calendar.getInstance();
		nowAdd28.add(Calendar.DATE, 27);
		boolean illegalDate;
		System.out.println("Departure Date and Time:(From now on 28 days are availible.)");
		do {
			System.out.print("Month:");
			int month = keyboard.nextInt();
			System.out.print("Day:");
			int day = keyboard.nextInt();
			System.out.print("Year:");
			int year = keyboard.nextInt();
			try {
				date.setTime(sdf.parse(year + "-" + month + "-" + day));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			illegalDate = date.after(nowAdd28) || date.before(now);
			if (illegalDate)System.out.println("Your request is illegal!");
		} while (illegalDate);
		System.out.println("Your input date:" + sdf.format(date.getTime()));
		for(TrainTable e:tt) {
			if(e.trainTableMatch(date)) {
				System.out.println(e);
			}
		}
	}
}
