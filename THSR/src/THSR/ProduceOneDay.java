package THSR;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;
import trainsystem.Station;
import trainsystem.Train;
/**
 * <h1>Produce list of trains for a day.</h1>
 * <p>
 * 
 * @author B05505001
 * @version 1.0
 * @since 2018-06-12
 */

public class ProduceOneDay {
	
	
	
	/**
	 * This method is used to compare two dates of string type weather the base is after the date.
	 * @param date The date of string to compare to.
	 * @param base The date of string to compare with.
	 * @return True(The date is after base);False(The date is before base).
	 */
	public static boolean dateAfter(String date,String base) {
		SimpleDateFormat d1 = new SimpleDateFormat("yyyy-MM-dd");
		Calendar date1 = Calendar.getInstance();
		Calendar base1 = Calendar.getInstance();
		Date temp1,temp2;
		try {
			temp1 = d1.parse(date);
			temp2 = d1.parse(base);
			date1.setTime(temp1);
			base1.setTime(temp2);
		} catch (ParseException e) {
			e.printStackTrace();
		} 
		return base1.before(date1);
	}
	
	/**
	 * Produce a list of the trains for a particular date.
	 * @param fileAddress Location of the file Timetable.json.
	 * @param date The particular date. 
	 * @return a list of the trains for a particular date.
	 */
	public static ArrayList<Train> produce(String fileAddress,Calendar date) {
		ArrayList<Train> trainList = new ArrayList<Train>();
		Train temp;
		SimpleDateFormat timeDate = new SimpleDateFormat("yyyy-MM-dd"),weekDay = new SimpleDateFormat("EEEE",Locale.ENGLISH);
		Date tempDate = date.getTime();
		BufferedReader bufferedReader;
		String tempS;
		StringBuffer stringBuffer = new StringBuffer();
		try {
			bufferedReader = new BufferedReader(new FileReader(fileAddress));
			try {
				while((tempS = bufferedReader.readLine())!=null) {
					stringBuffer.append(tempS);
				}
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
		} 
		catch (FileNotFoundException e2) {
			e2.printStackTrace();
		}
		tempS = stringBuffer.toString();
		JSONArray stopTimes,timeTable = new JSONArray(tempS);
		JSONObject train,generalTimeTable,trainInfo,stop,stationName,serviceDay,startingStationName,endingStationName;
		for(int i=0;i<(timeTable.length());i++) {
			temp = new Train();
			train = timeTable.getJSONObject(i);
			generalTimeTable = train.getJSONObject("GeneralTimetable");
			trainInfo = generalTimeTable.getJSONObject("GeneralTrainInfo");
			stopTimes = generalTimeTable.getJSONArray("StopTimes");
			serviceDay = generalTimeTable.getJSONObject("ServiceDay");
			startingStationName = trainInfo.getJSONObject("StartingStationName");
			endingStationName = trainInfo.getJSONObject("EndingStationName");
			if(serviceDay.getInt(weekDay.format(tempDate))!=1)continue;
			if(ProduceOneDay.dateAfter(timeDate.format(tempDate),train.getString("EffectiveDate"))!=true) {
				System.out.println("Unavailable date!");
				continue;
			}
			temp.setDateDepart(timeDate.format(tempDate));
			temp.setTrainNo(trainInfo.getString("TrainNo"));
			temp.setDirection(trainInfo.getInt("Direction"));
			temp.setStartEnd(startingStationName.getString("En"), endingStationName.getString("En"));
			for(int j=0;j<stopTimes.length();j++) {
				stop = stopTimes.getJSONObject(j);
				stationName = stop.getJSONObject("StationName");
				temp.setStop(stationName.getString("En"), stop.getString("DepartureTime"), j);
			}
			trainList.add(temp);
		}
		
		return trainList;
	}
	
	/**
	 * Testing.
	 * @param args
	 */
	public static void main(String[] args) throws CloneNotSupportedException, IOException {
		Calendar now = Calendar.getInstance();
		Station S = new Station("Taipei","Zuoying");
		ArrayList<Train> list = ProduceOneDay.produce("json/timeTable.json", now);
		for(int i=0;i<list.size();i++) {
			System.out.println((i+1)+".\n==Get Starting and Ending Station==");
			System.out.println(list.get(i).toString());
			System.out.println("===========Get Station============");
			for(int j=0;j<list.get(i).getNoOfStop();j++) {
				System.out.println((j+1)+"." + list.get(i).getStop(j).toString());
			}
			System.out.println("=========Check train match=========\nOrigin:Taipei,Destination:Zuoying,depart time:2018-6-11 15:45\n"+TrainMatch.trainMatch(list.get(i),now,0,S));
		}
	}
}
