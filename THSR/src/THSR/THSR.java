package THSR;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import org.json.JSONArray;
import org.json.JSONObject;

import trainsystem.IOjson;
import trainsystem.Station;

/**
 * Class that includes the main function and uses methods from other classes
 * 
 * @author Raymond
 */
public class THSR {
	public static void main(String[] args) {
		ArrayList<String> seats = new ArrayList<String>();
		JSONObject obj = new JSONObject();

		JSONObject obj1 = new JSONObject();
		JSONObject obj2 = new JSONObject();
		JSONArray Reservation = new JSONArray();
		seats.add("Car:6 Seat:1E");
		obj1.put("TrainNo", "0639");
		obj1.put("DepartureTime", new GregorianCalendar(2018, 6, 18, 3, 5).getTime());
		obj1.put("Name", "Adam");
		obj1.put("ID", "A123456789");
		obj1.put("Phone", "0800000000");
		obj1.put("Seat", seats);
		obj1.put("Class", "Standard");
		obj1.put("Station", new Station("Taipei", "Taoyuan"));
		obj1.put("OrderNo", 0);
		Reservation.put(obj1);
		seats.clear();
		seats.add("Car:6 Seat:2A");
		seats.add("Car:6 Seat:2E");
		obj2.put("TrainNo", "0639");
		obj2.put("DepartureTime", new GregorianCalendar(2018, 6, 18, 3, 5).getTime());
		obj2.put("Name", "Eve");
		obj2.put("ID", "A223456789");
		obj2.put("phone", "0800000001");
		obj2.put("Seat", seats);
		obj2.put("Class", "Standard");
		obj2.put("Station", new Station("Taipei", "Taoyuan"));
		obj2.put("OrderNo", 1);
		Reservation.put(obj2);
		obj.put("Reservations", Reservation);
		IOjson.write(obj);
		Booking mainSystem = new Booking();
		System.out.println("         ========System Start========\n");
		mainSystem.getAccess(Booking.produceAllTrain());
		System.out.println("\n         =========System End=========");
	}
}