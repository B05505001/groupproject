/**
 * 
 */
/**
 * Stores 2D-arrays of prices. They are stored in 2D-arrays since they can be
 * accessed easily with two indexes, origin and destination. This package also
 * includes different price tables for different kinds of tickets.
 * 
 * @author Raymond
 *
 */
package Price;