package Price;
/**
 * Prices for Discount Standard Class seats.
 * @author Raymond
 *
 */
public class PriceStandardD{
	public static int[][] priceTableStandardD = new int[][] {
	{0 ,20 ,35 ,100 ,165 ,240 ,375 ,435 ,485 ,560 ,695 ,765 },
	{20 ,0 ,20 ,80 ,145 ,215 ,350 ,410 ,465 ,540 ,675 ,745 },
	{35 ,20 ,0 ,65 ,130 ,200 ,335 ,395 ,450 ,525 ,660 ,730 },
	{100 ,80 ,65 ,0 ,65 ,140 ,270 ,335 ,390 ,460 ,595 ,665 },
	{165 ,145 ,130 ,65 ,0 ,70 ,205 ,270 ,320 ,395 ,530 ,600 },
	{240 ,215 ,200 ,140 ,70 ,0 ,135 ,195 ,250 ,320 ,460 ,530 },
	{375 ,350 ,335 ,270 ,205 ,135 ,0 ,65 ,115 ,190 ,325 ,395 },
	{435 ,410 ,395 ,335 ,270 ,195 ,65 ,0 ,55 ,125 ,265 ,335 },
	{485 ,465 ,450 ,390 ,320 ,250 ,115 ,55 ,0 ,75 ,210 ,280 },
	{560 ,540 ,525 ,460 ,395 ,320 ,190 ,125 ,75 ,0 ,140 ,205 },
	{695 ,675 ,660 ,595 ,530 ,460 ,325 ,265 ,210 ,140 ,0 ,70 },
	{765 ,745 ,730 ,665 ,600 ,530 ,395 ,335 ,280 ,205 ,70 ,0 },
	};
}
