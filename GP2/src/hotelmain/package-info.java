/**
 * 
 */
/**
 * The infrastructure of the system. Capable of handing up to 20 rooms of reservations for three kinds of pets.
 * 
 * @author Raymond
 *
 */
package hotelmain;