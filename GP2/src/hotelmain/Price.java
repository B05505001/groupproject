package hotelmain;

import java.util.ArrayList;

/**
 * This class solely calculates the total price due. Taking incidents that costed extra into account. 
 * 
 * @author Raymond
 *
 */
public class Price {
	public static int getPrice(int numOfDays, ArrayList<Incident> List) {
		int priceForIncidents = 0;
		for (int i = 0; i < List.size(); i++)
			priceForIncidents += List.get(i).getprice();
		return numOfDays * 500 + priceForIncidents;
	}
}
