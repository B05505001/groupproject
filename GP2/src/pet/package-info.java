/**
 * 
 */
/**
 * Info for different kinds of pets. The needs for each kind of pet varies,
 * therefore this package was created to respond to possible requests an owner might have.
 * 
 * @author Raymond
 *
 */
package pet;